package com.citi.training;

import com.citi.training.person.Person;

public class PersonMain {
    public static void main(String[] args) {
        Person person = new Person("Jose");

        System.out.println(person);
    }
}
