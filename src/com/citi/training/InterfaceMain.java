package com.citi.training;

import com.citi.training.person.Person;
import com.citi.training.pets.Dragon;

public class InterfaceMain {

    public static void main(String[] args) {

        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person horace = new Person("Horace");
        horace.setName("Horace O'Brien");

        Dragon smaug = new Dragon("Smaug");
        smaug.breathFire();
        thingsWeCanFeed[0] = horace;
        thingsWeCanFeed[0] = smaug;

        for (Feedable feedable : thingsWeCanFeed) {
            if (feedable != null) {
                feedable.feed();
            }
        }
    }

}
