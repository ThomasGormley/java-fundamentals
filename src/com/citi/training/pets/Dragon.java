package com.citi.training.pets;

import com.citi.training.Feedable;

public class Dragon extends Pet implements Feedable {

    public Dragon() {
        this.setNumLegs(4);
    }

    public Dragon(String name) {
        this();
        this.setName(name);
    }

    public void feed() {
        System.out.println("Feed dragon some dragon food");
    }

    public void breathFire() {
        System.out.println("🔥🔥🔥");
    }

    @Override
    public String toString() {
        return "This is a dragon with " + this.getNumLegs() + " and name: " + this.getName();
    }
}
