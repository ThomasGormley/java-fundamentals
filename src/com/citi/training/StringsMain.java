package com.citi.training;

import com.citi.training.pets.Dragon;

public class StringsMain {
    public static void main(String[] args) {

        String bigString = "start: ";
        StringBuilder stringBuilder = new StringBuilder(bigString);

        for (int i = 0; i < 10; i++) {
            stringBuilder.append(i);
        }

        System.out.println(stringBuilder);

        Dragon stringDragon = new Dragon("Smaug");

        System.out.println(stringDragon.toString());
    }
}
