package com.citi.training;

public interface Feedable {

    void feed();
}
